/**
  ******************************************************************************
  * @file      cmdParserLogFile.c
  * @brief      
  *            This module performs:
  *                - Init log file and Save log of last sessions 
  ******************************************************************************
**/

#include<stdio.h>
#include<string.h>
#include"cmdParserLogFile.h"



void InitLogFile(void)
{	FILE *fp;
	fp = fopen("logFile.txt","r");	//open file in read mode
	if(fp != NULL) //if file doesn't exists then create
	{	
	}
	else
	{	fclose(fp);
		fp = fopen("logFile.txt","w"); //open file in write mode
	}
	fclose(fp);
}
/**
  * @brief read old log
  			shift log
  			new log at top
*/
char strLog[MAX_LOG][64];
void SaveLog(char *strIn)
{	FILE *fp; int i = 0,j = 0,k = 0; char c; 
	fp = fopen("logFile.txt","r"); //open file in read mode
	if(fp != NULL)
	{ 	c = getc(fp);
		while(c != EOF)	//save data to array
		{	strLog[i][j++] = (char)c;
			if(c == '\n' ) { strLog[i][j] = 0; j=0; i++; }			
			c = (char)getc(fp);
		} 
		
		
	 	if(i > MAX_LOG-1) i = MAX_LOG-1;
		for(j=i;j>0;j--)  strcpy(strLog[j],strLog[j-1]);	//shift log newwest log at top
		strcpy(strLog[0],strIn);	//Newest log
		strLog[0][strlen(strIn)] = '\n'; strLog[0][strlen(strIn)+1] = 0;	
		fclose(fp); 
		
		//rewrite files
		
		fp = fopen("logFile.txt","w"); //open file in write mode 
		if(fp != NULL)
		{	for(j=0;j<=i;j++)
			{	for(k=0;k<64;k++)	
				{	putc(strLog[j][k],fp);
					if((strLog[j][k] == '\n') || (strLog[j][k] == 0)) break; 
				}
			}
		}
	}
	else //if first time file is not created
	{	printf("Error: logFile.txt missing");  
	} 
	fclose(fp);
}


