 
### What is this repository for? ###
A simulator of a communication protocol with an electric motor. This is aconsole application, which accept motor commands from user input and from batch file, parse the command, send it to the motor, get response from the motor and report the result to the console.
Command parser and motor emulator are separate modules in the project. 

### Version ###
MotorEmulator_v1.0 : working with  windows and linux
MotorEmulator_v1.1 : No major changes except few comments

### Who do I talk to? ###
Rahul Singh Bhauryal
rahulbhauryal@hotmail.com
