#ifndef _FILE
#define _FILE

#define FAIL		0
#define SUCCESS		1
#define PROFILE_NA	2
void InitFiles(void);
void SaveCurrent(void);
void SaveProfile(int , int , int );
int ReadProfile(int ,unsigned int* ,unsigned int* );
#endif


