#ifndef _TX
#define _TX

typedef struct
{	int Flag; int Val;
}CMD_ACK; 

void CmdParserInit(void);
int CommandResolver(char *);
int CPReadResponse(unsigned int*);
void FillCmdParserRxBuf(unsigned char );
void SendCommand(char , unsigned int , unsigned char , unsigned int, int );
CMD_ACK ProcessObject(char , unsigned int , unsigned char , unsigned int );
void ShowErrWarning(int );
void SaveLog(char *);
void CommandAssigner(int Cmd, char *);
void ProcessCmdParser(char *);
int SearchNProcessTxtFile(char *);
int ExtractValAfterSpace(char *, int);

#endif

